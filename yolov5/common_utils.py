import math
import requests
import cv2
import base64
import os
from abc import ABC, abstractmethod
import os
from bson.objectid import ObjectId
import shutil
from kafka import KafkaConsumer, TopicPartition, KafkaProducer
import json
import torch
import numpy as np
import redis
import pickle
from pymongo import MongoClient
from settings import *
from detect_mod2 import *
from correcting_annotations import *
import pysftp
from google.cloud import storage


#os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'first-campaign-346416-e263efdbed96.json'

def create_directories(base_dir):
    data_dir = base_dir + "/data/parts_data"
    os.mkdir(data_dir)  # ******************
    os.mkdir(data_dir + "/images")
    os.mkdir(data_dir + "/images/train")
    os.mkdir(data_dir + "/images/val")
    os.mkdir(data_dir + "/labels")
    os.mkdir(data_dir + "/labels/val")
    os.mkdir(data_dir + "/labels/train")  # *********
    return data_dir


def create_data_and_yaml(part_id, data_dir, domain, class_labels_dict):
    mp = MongoHelper().getCollection(str(part_id) + '_dataset', domain=domain)
    objs = [i for i in mp.find({"state": "tagged"})]
    print(len(objs))
    train_len = math.ceil(len(objs) * 0.8)
    val_len = abs(int(len(objs)) - train_len)
    print("Length of train dataset is " + str(train_len))
    print("*****")
    print("Length of validation dataset is " + str(val_len))
    print("..Building the dataset..")
    count = 0
    ### Rendering Image size from dataset
    # document = objs[0]
    document = objs[1]
    # print("**********************",document)
    file_url = document["file_url"]
    r = requests.get(file_url)
    img_name = document["file_path"].split('/')[-1]
    print(img_name)

    if document["state"] == "tagged":
        with open(img_name, 'wb') as f:
            f.write(r.content)
        f.close()
    img = cv2.imread(img_name)
    h, w, _ = img.shape
    
    # Creating annotations
    for document in objs:
        file_url = document["file_url"]
        r = requests.get(file_url)
        if count <= val_len:
            temp_dir_images = data_dir + "/images/val/"
            temp_dir_labels = data_dir + "/labels/val/"
        elif count > val_len & count <= train_len:
            temp_dir_images = data_dir + "/images/train/"
            temp_dir_labels = data_dir + "/labels/train/"
        else:
            break
        img_name = document["file_path"].split('/')[-1]
        label_name = str((img_name.split('.')[0])) + ".txt"
        image_file_path = os.path.join(temp_dir_images, img_name)
        label_file_path = os.path.join(temp_dir_labels, label_name)

        if document["state"] == "tagged" and document["annotator"] == "":
            with open(image_file_path, 'wb') as f:
                f.write(r.content)
            f.close()

            if 'annotation_detection' in document and len(document['annotation_detection']) > 0:
                g = open(label_file_path, "+a")
                for entry in document['annotation_detection']:

                    if entry["type"] == "box":
                        annots_list = [entry["x"], entry["y"], entry["w"], entry["h"]]

                        bb = driver((h, w), annots_list)
                        class_key = entry["cls"]
                        class_value = class_labels_dict[class_key]
                        line = str(class_value) + " " + str(bb[0]) + " " + str(bb[1]) + " " + str(bb[2]) + " " + str(
                            bb[3]) + "\n"
                        g.write(line)
                g.close()
        elif document["state"] == "tagged" and document["annotator"] == "upload":
            with open(image_file_path, 'wb') as f:
                f.write(r.content)
            f.close()

            if 'annotation_detection' in document and len(document['annotation_detection']) > 0:
                g = open(label_file_path, "+a")
                for entry in document['annotation_detection']:

                    if entry["type"] == "box":
                        x0 = entry["x"]
                        y0 = entry["y"]
                        w = entry["w"]
                        h = entry["h"]
                        class_key = entry["cls"]
                        class_value = class_labels_dict[class_key]
                        line = str(class_value) + "  " + str(x0) + " " + str(y0) + " " + str(w) + " " + str(h) + "\n"
                        g.write(line)
                g.close()

        count = count + 1

    class_labels_list = [key for key, value in class_labels_dict.items()]
    yaml_file = data_dir + ".yaml"
    file = open(yaml_file, "a+")
    file.write("train: data/parts_data/images/train\n")
    file.write("val: data/parts_data/images/val\n\n")
    file.write("nc: " + str(len(class_labels_dict)) + "\n\n")
    file.write("names: " + str(class_labels_list))
    file.close()
    return 0


def get_latest_threshold(workstation_id):
    rch = CacheHelper()
    # threshold = float(rch.get_json("threshold_"+data["exp_id"]))
    threshold = rch.get_json("threshold_" + str(workstation_id))
    if not threshold:
        threshold = 0.3
    else:
        threshold = float(threshold)
        print("*******Threshold updated*****")
        print(threshold)
    return threshold


def imEncoder(image):
    cv2.imwrite("temp.jpg", image)
    with open("temp.jpg", 'rb') as f:
        im_b64 = base64.b64encode(f.read())
    return str(im_b64)


def imDecoder(img_str):
    im_b64 = bytes(img_str[2:], 'utf-8')
    im_binary = base64.b64decode(im_b64)
    im_arr = np.frombuffer(im_binary, dtype=np.uint8)
    frame = cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)
    return frame

def single_file_transfer(bucket_name, input_file_name, output_file_name):
    def list_blobs(bucket_name):
        bucket_con = []
        """Lists all the blobs in the bucket."""
        # bucket_name = "your-bucket-name"
        storage_client = storage.Client()
        # Note: Client.list_blobs requires at least package version 1.17.0.
        blobs = storage_client.list_blobs(bucket_name)
        for blob in blobs:
            # print(blob.name)
            bucket_con.append(blob.name)
        return bucket_con

    # To list all contents in the bucket
    buckt_contents = list_blobs(bucket_name)

    """
    Upload File
    """

    def upload_to_bucket(blob_name, file_path, bucket_name):
        '''
        Upload file to a bucket
        : blob_name  (str) - object name
        : file_path (str)
        : bucket_name (str)
        '''
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(blob_name)
        blob.upload_from_filename(file_path)
        return blob.public_url

    print(os.getcwd())
    storage_client = storage.Client()

    print("Copying File name:", input_file_name)
    upload_to_bucket(output_file_name, input_file_name, bucket_name)
    updated_buckt = list_blobs(bucket_name)
    print("Done uploading to ", output_file_name)


def upload_file(file_path, part_id):
    single_file_transfer("livis_parts", file_path, part_id+"/"+file_path)
    return 0




def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return getinstance


@singleton
class CacheHelper():
    def __init__(self):
        # self.redis_cache = redis.StrictRedis(host="13.235.133.102", port=5051, db=0, socket_timeout=1)
        self.redis_cache = redis.StrictRedis(host=redis_host, port=redis_port, db=0, socket_timeout=1)

    def get_redis_pipeline(self):
        return self.redis_cache.pipeline()

    def set_json(self, dict_obj):
        try:
            k, v = list(dict_obj.items())[0]
            v = pickle.dumps(v)
            return self.redis_cache.set(k, v)
        except redis.ConnectionError:
            return None

    def get_json(self, key):
        try:
            temp = self.redis_cache.get(key)

            if temp:
                temp = pickle.loads(temp)
            return temp
        except redis.ConnectionError:
            return None
        return None

    def execute_pipe_commands(self, commands):
        return None


@singleton
class MongoHelper:
    client = None

    def __init__(self):
        if not self.client:
            self.client = MongoClient(host=MONGO_SERVER_HOST, port=MONGO_SERVER_PORT)
        self.db = self.client[MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None, domain="autent"):
        _DB = MONGO_DB
        DB = self.client[_DB]
        cname = domain + cname
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]

class DataMover(ABC):  ## can be used for moving data

    def __init__(self):
        self.client = None

    @abstractmethod
    def single_file_transfer(self):
        return


@singleton
class GCPHelper(DataMover):
    def __init__(self):
        super().__init__()
        if not self.client:
            os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'first-campaign-346416-e263efdbed96.json'
            self.client = storage.Client()
        self.bucket_name = ""

    def set_bucket_name(self, name):
        self.bucket_name = name

    def list_blobs(self):
        bucket_con = []
        """Lists all the blobs in the bucket."""
        # Note: Client.list_blobs requires at least package version 1.17.0.
        blobs = self.client.list_blobs(self.bucket_name)
        for blob in blobs:
            # print(blob.name)
            bucket_con.append(blob.name)
        return bucket_con

    def upload_to_bucket(self, blob_name, file_path):
        '''
        Upload file to a bucket
        : blob_name  (str) - object name
        : file_path (str)
        : bucket_name (str)
        '''
        bucket = self.client.get_bucket(self.bucket_name)
        blob = bucket.blob(blob_name)
        blob.upload_from_filename(file_path)
        return blob.public_url

    def single_file_transfer(self, input_file_name, output_file_name):
        print("Copying File name:", input_file_name)
        self.upload_to_bucket(output_file_name, input_file_name)



# Create the Predictor Abstract class
# @singleton
# Parent class
class ModelArchitecture(ABC):  ## can be used to implement different AI model architectures like yolo, tensorflow etc.

    def __init__(self):
        self.data = None
        self.config = {}

    @abstractmethod
    def preprocess_data_for_train(self):
        return

    @abstractmethod
    def run_inference(self):
        return

    # @abstractmethod
    # def postprocess_data(self):
    #     return


@singleton
class Yolo(ModelArchitecture):

    def __init__(self):
        super().__init__
        # self.streamer --> class to be created
        self.streamer = None
        self.mount_drive = mount_drive  # from settings
        json_path = self.mount_drive + "parameters.json"  # $$
        with open(json_path, "r") as file:
            self.config = json.loads(file.read())
        file.close()
        self.experiment_id = self.config["exp_id"]
        self.weights = self.config["hyperparameters"]["weights"]
        self.retrain = self.config["retrain"]
        self.domain = self.config["domain"]
        self.hyperparameters = self.config["hyperparameters"]

    def train(self):
        mp = MongoHelper().getCollection('experiment', domain=self.domain)
        batch_size = self.hyperparameters["batch_size"]
        epochs = self.hyperparameters["epochs"]
        img_size = self.hyperparameters["image_size"]
        scratch_file = self.mount_drive + "hyp.scratch.yaml"
        if self.hyperparameters.get("hyp"):
            scratch_contents = self.hyperparameters["hyp"]
            with open(scratch_file, "w+") as f:
                f.write(scratch_contents)
            f.close()
        image_weights = ""
        label_smoothing = 0.0
        if self.hyperparameters.get("image-weights"):
            image_weights = " --image-weights "
        if self.hyperparameters.get("label-smoothing"):
            label_smoothing = self.hyperparameters["label-smoothing"]

        train_file_path = os.getcwd() + "/train.py"
        data_path = os.getcwd() + "/data/parts_data.yaml"

        if not self.retrain:
            train_command = "python3  " + train_file_path + " --img " + str(img_size) + " --batch-size " + str(
                batch_size) \
                            + " --data " + data_path + "  --weights " + self.weights + ".pt  --epochs " + str(epochs) + \
                            " --device 0  --adam  --experiment_id " + str(self.experiment_id) + " --hyp " + str(
                scratch_file) + image_weights + " --label-smoothing " + str(label_smoothing) \
                            + " --domain " + str(self.domain)

        else:
            print("Preparing for retrain")
            weights_path = self.mount_drive + "runs/train/exp/weights/best.pt"
            train_command = "python3  " + train_file_path + " --img " + str(img_size) + " --batch-size " + str(
                batch_size) \
                            + " --data " + data_path + "  --weights " + weights_path + " --epochs " + str(epochs) + \
                            " --device 0  --adam  --experiment_id " + str(self.experiment_id) + " --hyp " + str(
                scratch_file) + image_weights + " --label-smoothing " + str(label_smoothing) \
                            + " --domain " + str(self.domain)

        print("in training block")
        try:
            print(train_command)
            os.system(train_command)
            experiment = mp.find_one({"_id": ObjectId(self.experiment_id)})
            experiment["status"] = "Completed"
            f = open("final_results.txt", "r")
            map95 = f.read()
            f.close()
            experiment["yolo_final_MAP"] = map95
            mp.update_one({'_id': experiment['_id']}, {'$set': experiment})
            ## moving weights and results
            source = "runs/train/exp/"
            destination = self.mount_drive + "runs/train/exp/"
            if os.path.exists(destination):
                command = "rm -rf " + str(destination)
                os.system(command)
            shutil.copytree(source, destination)
            ## moving data and yaml file
            source_data = "data/parts_data/"
            destination = self.mount_drive + "data/parts_data/"
            if os.path.exists(destination):
                command = "rm -rf " + str(destination)
                os.system(command)
            shutil.copytree(source, destination)
            command = "rm -rf data/parts_data/*"
            os.system(command)

            # Uploading datadrive folder with weights to gcp
            src_file_path = self.mount_drive+"parameters.json"
            gcpMover = GCPHelper()
            gcpMover.set_bucket_name("livis_datadrive")
            gcpMover.single_file_transfer(src_file_path, str(self.experiment_id)+"/parameters.json" )
            
            src_file_path_runs = self.mount_drive+"runs"

            # for subdir, dirs, files in os.walk(src_file_path_runs):
            #     for file in files:
            #         print(os.path.join(subdir, file))

            for subdir, dirs, files in os.walk(src_file_path_runs):
                
                for file in files:
                    print(file)
                    print(" subdir ",subdir)
                    strIdx = subdir.find("runs")

                    DestinationPath = str(self.experiment_id)+"/"+subdir[strIdx:]
                    print("src ",os.path.join(subdir, file), " gcp dest ",  DestinationPath)
                    print("\n\n")
                    gcpMover.single_file_transfer(os.path.join(subdir, file), os.path.join(DestinationPath, file))

            # os.system(cmd)

        except Exception as e:
            print(e)
            experiment = mp.find_one({"_id": ObjectId(self.experiment_id)})
            experiment["status"] = "Failed"
            mp.update_one({'_id': experiment['_id']}, {'$set': experiment})
        return 0

    def preprocess_data_for_train(self):
        # Creating folders for train data
        base_dir = os.getcwd()
        data_dir = create_directories(base_dir)
        part_id = self.config["part_id"]
        domain = self.config["domain"]

        mp = MongoHelper().getCollection(str(part_id) + '_dataset', domain=domain)
        temp_cls_list = []
        objs = [i for i in mp.find()]

        for document in objs:
            if document["state"] == "tagged" and 'annotation_detection' in document and \
                    len(document['annotation_detection']) > 0:
                for entry in document['annotation_detection']:
                    if entry['cls'] not in temp_cls_list:
                        temp_cls_list.append(entry['cls'])

        class_labels_dict = {}
        for item_ in temp_cls_list:
            class_labels_dict[item_] = int(temp_cls_list.index(item_))
        ## Generating the dataset
        print("Generating the dataset")
        create_data_and_yaml(part_id, data_dir, domain, class_labels_dict)
        return 0

    def run_inference_live(self):
        consumer = KafkaConsumer(bootstrap_servers=[self.config["kafka_url"]], auto_offset_reset="latest")
        producer = KafkaProducer(bootstrap_servers=self.config["kafka_url"])
                                 #value_serializer=lambda value: json.dumps(value).encode(),
                                 #max_request_size=41943040000000000000000)
        input_topic = self.config["topic"] + "_i"
        output_topic = self.config["topic"] + "_o"
        tp = TopicPartition(input_topic, 0)
        consumer.assign([tp])
        consumer.seek_to_end(tp)
        last_offset = consumer.position(tp)
        print("Topics:", input_topic)
        # threshold = 0.3
        #threshold = get_latest_threshold(self.config["exp_id"])
        threshold = get_latest_threshold(self.config["workstation_id"])
        img_size = self.config["hyperparameters"]["image_size"]
        inf_deploy = self.config["inf_deployment"]
        weights = self.config["trained_weights"]
        #weights = mount_drive+"runs/train/exp/weights/best.pt" ##for testing
        if self.config.get("feature_list"):
            feature_list = self.config.get("feature_list")
        else:
            feature_list = []
        if self.config.get("defect_list"):
            defect_list = self.config.get("defect_list")
        else:
            defect_list = []

        if inf_deploy == 'gpu':
            device = '0'
        else:
            device = 'cpu'
        ## load model from detect_mod2.py
        model, imgsz, stride, names, pt, jit = load_model(weights=weights, device=device, imgsz=int(img_size))

        with torch.no_grad():
            experiment_id = self.config["exp_id"]
            mp = MongoHelper().getCollection('experiment', domain=self.domain)
            experiment = mp.find_one({"_id": ObjectId(experiment_id)})
            experiment['status'] = "Running_inference"
            mp.update_one({'_id': experiment['_id']}, {'$set': experiment})

            while True:
                for msg in consumer:
                    #image = json.loads(msg.value.decode('utf-8'))
                    #img = imDecoder(image["frame"])
                    img = cv2.imdecode(np.frombuffer(msg.value, np.uint8), 1)

                    cv2.imwrite("input_frame.jpg", img)
                    threshold = get_latest_threshold(self.config["exp_id"])
                    ## run model from detect_mod2.py
                    label_list, im0 = run_model("input_frame.jpg", model, device, imgsz, stride, names, pt, jit, threshold,
                                           feature_list=feature_list, defect_list=defect_list)
                    #output_frame = cv2.imread("predict.jpg")
                    #ouput_frame = im0
                    #output = imEncoder(output_frame)
                    encoded_output = cv2.imencode('.jpeg', im0)[1].tobytes()
                    #output_data = {"frame": output, "predictions": label_list}
                    producer.send(topic=output_topic,key=bytes(str(label_list),'utf-8'), value=encoded_output)
                    consumer.seek_to_end(tp)
        return 0

    def run_inference_on_video(self):
        print("starting predictions on preview video")
        mp = MongoHelper().getCollection(self.config['part_id'] + "_static_dataset", domain=self.domain)
        colle = [i for i in mp.find().sort("_id", -1).limit(1)][0]

        inf_deploy = self.config["inf_deployment"]
        file_path = colle["file_url"]  ######
        print("input file path", file_path)
        print("downloading the file into container")
        print(file_path)
        r = requests.get(file_path)
        if self.config.get("feature_list"):
            feature_list = self.config.get("feature_list")
        else:
            feature_list = []
        if self.config.get("defect_list"):
            defect_list = self.config.get("defect_list")
        else:
            defect_list = []
        img_name = colle["file_url"].split('/')[-1]
        print("input file", img_name)

        if inf_deploy.find('gpu') > -1:
            device = 0
        else:
            device = 'cpu'

        if img_name.find(".jpg") > -1 or img_name.find(".png") > -1:
            output_file = img_name.split(".")[0]
            output_file = "predict." + img_name.split(".")[-1]
            print("output_file", output_file)
            with open("input_frame.jpg", 'wb') as f:
                f.write(r.content)
            f.close()
            threshold = 0.3
            number_of_frames = 0
            img_size = self.config["hyperparameters"]["image_size"]
            model = None
            weights = self.config["trained_weights"]
            #weights = mount_drive+"runs/train/exp/weights/best.pt" ##for testing
            print("weights", weights)
            print("loading model for inference")

            model, imgsz, stride, names, pt, jit = load_model(weights=weights, device=device, imgsz=int(img_size))
            with torch.no_grad():
                experiment_id = self.config["exp_id"]
                mp = MongoHelper().getCollection('experiment', domain=self.domain)
                experiment = mp.find_one({"_id": ObjectId(experiment_id)})
                experiment['status'] = "Running_inference"
                mp.update_one({'_id': experiment['_id']}, {'$set': experiment})
                print("Performing prediction on image and storing image")
                label_list, im0 = run_model("input_frame.jpg", model, device, imgsz, stride, names, pt, jit, threshold,
                                       feature_list=feature_list, defect_list=defect_list)
                # output_frame = cv2.imread("predict.jpg")
                output_frame = im0

        else:

            output_file = img_name.split(".")[0]
            output_file = output_file + "_output." + img_name.split(".")[-1]
            print("output_file", output_file)
            f = open(img_name, 'wb+')
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
            f.close()
            cap = cv2.VideoCapture(img_name)
            frame_width = int(cap.get(3))
            frame_height = int(cap.get(4))
            size = (frame_width, frame_height)
            result_video = cv2.VideoWriter(output_file,
                                           cv2.VideoWriter_fourcc(*'MJPG'),
                                           10, size)

            threshold = 0.3
            number_of_frames = 0
            img_size = self.config["hyperparameters"]["image_size"]
            model = None
            weights = self.config["trained_weights"]
            #weights = mount_drive+"runs/train/exp/weights/best.pt"
            print("weights", weights)
            device = 0
            print("loading model for inference")

            model, imgsz, stride, names, pt, jit = load_model(weights=weights, device=device, imgsz=int(img_size))

            with torch.no_grad():
                experiment_id = self.config["exp_id"]
                mp = MongoHelper().getCollection('experiment', domain=self.domain)
                experiment = mp.find_one({"_id": ObjectId(experiment_id)})
                experiment['status'] = "Running_inference"
                mp.update_one({'_id': experiment['_id']}, {'$set': experiment})
                print("Performing predictions and storing video")

                while (cap.isOpened()):
                    # Capture frame-by-frame
                    ret, frame = cap.read()
                    if ret == True and number_of_frames <= 1500:
                        cv2.imwrite("input_frame.jpg", frame)
                        label_list, im0 = run_model("input_frame.jpg", model, device, imgsz, stride, names, pt, jit,
                                               threshold,
                                               feature_list=feature_list, defect_list=defect_list)
                        number_of_frames += 1
                        # output_frame = cv2.imread("predict.jpg")
                        result_video.write(im0)

                    else:
                        break
                cap.release()
                result_video.release()
        print("inference completed")
        ## 
        #upload_file(output_file, self.config['part_id'])
        gcpMover = GCPHelper()
        gcpMover.set_bucket_name("livis_parts")
        target_file = self.config['part_id'] + "/" + output_file
        print(" ** Inference done ")
        print("uploading source ",output_file, " to target ", target_file)
        gcpMover.single_file_transfer(output_file, target_file)
        mp = MongoHelper().getCollection("parts", domain=self.domain)
        part_colle = mp.find_one({'_id': ObjectId(self.config['part_id'])})
        file_url = "https://storage.googleapis.com/livis_parts/"+self.config['part_id'] + "/" + output_file
        part_colle["preview_url"] = file_url
        mp.update_one({'_id': ObjectId(self.config['part_id'])}, {"$set": part_colle})
        return 0

    def run_inference_on_mobile_image(self):
        print("run_inference_mobile_image")
        rch = CacheHelper()
        threshold = get_latest_threshold(self.config["exp_id"])

        img_size = self.config["hyperparameters"]["image_size"]
        inf_deploy = self.config["inf_deployment"]  # CPU /GPU
        weights = self.config["trained_weights"]
        # weights = mount_drive+"runs/train/exp/weights/best.pt"  ##for testing

        part_id = self.config["part_id"]
        if self.config.get("feature_list"):
            feature_list = self.config.get("feature_list")
        else:
            feature_list = []
        if self.config.get("defect_list"):
            defect_list = self.config.get("defect_list")
        else:
            defect_list = []

        if inf_deploy == "cpu":
            device = "cpu"
        else:
            device = ''

        if self.config["mobile_id"]:
            mobile_id = self.config["mobile_id"]
        else:
            mobile_id = 0
        mp = MongoHelper().getCollection('experiment', domain=self.domain)
        experiment = mp.find_one({"_id": ObjectId(self.experiment_id)})
        experiment['status'] = "Running_inference"
        mp.update_one({'_id': experiment['_id']}, {'$set': experiment})

        # Initialize
        mobile_trigger = False
        inspection_completed = False
        rch.set_json({f"{mobile_id}_inspection_completed": False})
        rch.set_json({f"{mobile_id}_mobile_trigger": False})
        with torch.no_grad():
            # Load model
            model, imgsz, stride, names, pt, jit = load_model(weights=weights, device=device, imgsz=int(img_size))
            while True:
                # get mobile trigger
                mobile_trigger = rch.get_json(f"{mobile_id}_mobile_trigger")  # Bluedoor_checkpoint_1_paramter_2
                inspection_completed = rch.get_json(f"{mobile_id}_inspection_completed")
                threshold = get_latest_threshold(self.config["exp_id"])

                if (mobile_trigger == True) & (inspection_completed == False):
                    print("Received trigger")
                    zone_data = rch.get_json(f"{mobile_id}_zone_data")
                    print(zone_data)
                    parameter = zone_data["parameter"]
                    # if mobile_trigger and parameter:
                    predicted_frame_dict_list = {}
                    prediction_dict_list = {}
                    print(part_id, parameter, str(part_id) == str(parameter))
                    if str(part_id) == str(parameter):
                        print("part_id == parameter satisfied!!")
                        image_list = rch.get_json(f"{mobile_id}_input_frame_list")
                        print("Length of frame list:::", len(image_list))
                        for e, image in enumerate(image_list):
                            # cv2.imwrite(f"{e}.jpg",image)
                            print(e)
                            label_list, predicted_image = run_model_image(image, model, device, imgsz, stride, names,
                                                                          pt,
                                                                          jit, conf_thres=threshold,
                                                                          feature_list=feature_list,
                                                                          defect_list=defect_list)
                            print(label_list)
                            predicted_image = cv2.resize(predicted_image,
                                                         (predicted_image.shape[0] // 2, predicted_image.shape[1] // 2))
                            predicted_frame_dict_list[e] = predicted_image
                            cv2.imwrite(f"{e}.jpg", predicted_image)
                            print("predicted_image:::", predicted_image.shape)
                            prediction_dict_list[e] = label_list
                        print("inference_completed!!!!!!!!!!!!!!!!!")
                        try:
                            print("prediction_dict_list::", prediction_dict_list)
                            rch.set_json({f"{mobile_id}_output_frame_list": predicted_frame_dict_list})
                            rch.set_json({f"{mobile_id}_label_list": prediction_dict_list})
                            rch.set_json({f"{mobile_id}_inspection_completed": True})
                            rch.set_json({f"{mobile_id}_mobile_trigger": False})
                            print("wrote to redis!!!!!!!!!!!!!")
                        except Exception as e:
                            print(e)
                            print("prediction_dict_list::", prediction_dict_list)
                            rch.set_json({f"{mobile_id}_output_frame_list": predicted_frame_dict_list})
                            rch.set_json({f"{mobile_id}_label_list": prediction_dict_list})
                            rch.set_json({f"{mobile_id}_inspection_completed": True})
                            rch.set_json({f"{mobile_id}_mobile_trigger": False})
                            print("wrote to redis!!!!!!!!!!!!!")
                            pass
                else:
                    continue
        return 0

    def run_inference(self):
        if self.config["inference_mode"] == "live":
            self.run_inference_live()
        elif self.config["inference_mode"] == "static":
            self.run_inference_on_video()
        elif self.config["inference_mode"] == "mobile":
            self.run_inference_on_mobile_image()
