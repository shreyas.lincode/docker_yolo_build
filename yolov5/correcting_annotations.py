import os
import cv2

def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)

# file_list = os.listdir()

# annot_list = [i for i in file_list if i.find("txt") > -1]

# print(annot_list)


# for file_ in annot_list:
#     print(file_)
#     f = open(file_,"r")
#     str_ = f.read()
#     f.close()
#     print("Before")
#     print(str_)
#     str_ = str_.replace("\n","")
#     annots = str_.split(" ")
#     class_ = annots[0]
#     img_file = file_.replace("txt", "png")
#     img = cv2.imread(img_file)
#     h,w,_ = img.shape
#     x,y,dx,dy =0,0,0,0
#     x= round(float(annots[1]),2)*w
#     y = round(float(annots[2]),2)*h
#     dx = round(float(annots[3]),2)*w
#     dy = round(float(annots[4]),2)*h
#     points = [(x,y), (x+dx, y), (x,y+dy), (x+dx,y+dy)]
#     xmax = x+dx
#     ymax = y+dy
#     b = (x,xmax,y, ymax)
#     bb = convert((w,h), b)
#     f= open("annots/"+str(file_), "w")
#     str_ = class_+" "+str(bb[0])+" "+str(bb[1])+" "+str(bb[2])+" "+str(bb[3])+"\n"
#     f.write(str_)
#     f.close()

def driver(size, regions):
    h = size[0]
    w = size[1]
    x,y,dx,dy =0,0,0,0
    x= round(float(regions[0]),2)*w
    y = round(float(regions[1]),2)*h
    dx = round(float(regions[2]),2)*w
    dy = round(float(regions[3]),2)*h
    points = [(x,y), (x+dx, y), (x,y+dy), (x+dx,y+dy)]
    xmax = x+dx
    ymax = y+dy
    b = (x,xmax,y, ymax)
    bb = convert((w,h), b)
    return bb
    








