from common_utils import *


if __name__ == '__main__':
    # Instantiate the yolo predictor class
    yolo_obj = Yolo()

    # If task is training of experiment
    if yolo_obj.config["hyperparameters"]["task"] == "train":
        mp = MongoHelper().getCollection('experiment', domain=yolo_obj.domain)
        experiment = mp.find_one({"_id": ObjectId(yolo_obj.config["exp_id"])})
        experiment['status'] = "Running"
        mp.update_one({'_id': experiment['_id']}, {'$set': experiment})
        yolo_obj.preprocess_data_for_train() 
        print("Training and updating the experiments collection")
        yolo_obj.train()

    # If task is inference on live stream
    elif yolo_obj.config["hyperparameters"]["task"] == "inference":
        print("inside >>>>>> inference>>>>>>>")
        mp = MongoHelper().getCollection('experiment', domain=yolo_obj.domain)
        experiment = mp.find_one({"_id": ObjectId(yolo_obj.config['exp_id'])})
        experiment['status'] = "Loading"
        mp.update_one({'_id': experiment['_id']}, {'$set': experiment})
        print('experiment["inference_mode"] :::::::::::::', experiment["inference_mode"])
        yolo_obj.run_inference()
