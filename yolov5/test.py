
from tqdm import tqdm
import pysftp




def viewBar(a,b):
    # original version
    res = a/int(b)*100
    sys.stdout.write('\rComplete precent: %.2f %%' % (res))
    sys.stdout.flush()

def tqdmWrapViewBar(*args, **kwargs):
    try:
        from tqdm import tqdm
    except ImportError:
        # tqdm not installed - construct and return dummy/basic versions
        class Foo():
            @classmethod
            def close(*c):
                pass
        return viewBar, Foo
    else:
        pbar = tqdm(*args, **kwargs)  # make a progressbar
        last = [0]  # last known iteration, start at 0
        def viewBar2(a, b):
            pbar.total = int(b)
            pbar.update(int(a - last[0]))  # update pbar with increment
            last[0] = a  # update last known iteration
        return viewBar2, pbar  # return callback, tqdmInstance



PATH_TO_KEY = "livis-kp.pem"
DESTINATION_FOLDER = "/datadrive/inference_video/"

def upload(file_path, destination_folder):
    fname = file_path.split('/')[-1]
    with pysftp.Connection(host='13.234.202.181', username='ubuntu', private_key=PATH_TO_KEY) as sftp:
        with sftp.cd(DESTINATION_FOLDER):           # temporarily chdir to allcode
            cbk, pbar = tqdmWrapViewBar(ascii=True, unit='b', unit_scale=True)
            sftp.put(fname, callback=cbk)  	# upload file to allcode/pycode on remote
            
upload("phillips_output.mp4")