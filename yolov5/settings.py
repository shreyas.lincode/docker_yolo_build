import json

## When shifting to server line 24, create dir uncomment, add device 0 for gpu support
mount_drive = "/datadrive/"  ##*******
#mount_drive = "/datadrive/models/6194d6a6cf479547e49f0583"
json_path = mount_drive + "/parameters.json"
with open(json_path, "r") as file:
    data = file.read()

data = json.loads(data)
kafka_url = data["kafka_url"]
MONGO_SERVER_HOST = data["mongo_server_host"]
MONGO_SERVER_PORT = data["mongo_server_port"]
MONGO_DB = data["mongo_db_name"]
MONGO_COLLECTION_PARTS = data["mongo_collection_parts"]
redis_host = data["redis_host"]
redis_port = data["redis_port"]
datadrive_host = data["datadrive_host"]
datadrive_port = data["datadrive_port"]
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: MONGO_COLLECTION_PARTS}
