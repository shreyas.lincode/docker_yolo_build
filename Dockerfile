#FROM nvidia/cuda:11.1-cudnn8-runtime-ubuntu18.04
FROM ritikaniyengar/base_yolo
ENV PYTHONIOENCODING='utf8'

#RUN apt update \
#     && apt install -y htop gcc python3.7-dev vim git wget libevent-dev libcairo2-dev pkg-config \
#     protobuf-compiler python-pil python-lxml  libgl1-mesa-glx && \
#     apt install -y python3-pip && pip3 install --upgrade pip 

# Create working directory
#RUN mkdir -p /datadrive
#RUN mkdir -p /yolo
WORKDIR /yolo

# Copy contents
COPY . /yolo

# Set environment variables
ENV HOME=/yolo

#RUN pip3 install kafka-python
RUN pip3 install redis
RUN pip3 install pysftp
RUN pip3 uninstall numpy -y
RUN pip3 install numpy

RUN pip3 install -r gcp_requirements.txt

#RUN pip3 install torch==1.9.0+cu111 torchvision==0.10.0+cu111 -f https://download.pytorch.org/whl/torch_stable.html

#RUN export LD_LIBRARY_PATH=/usr/local/cuda/lib64

#RUN cp /usr/local/cuda/lib64/libcusolver.so.11 /usr/local/cuda/lib64/libcusolver.so.10

CMD /bin/bash -c  "cd /yolo/yolov5 && python3 operations.py"
